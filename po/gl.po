# translation of gl.po to Galego
# This file is distributed under the same license as the epiphany-extensions package.
# Copyright (C) 2003 The Free Software Foundation.
# Ignacio Casal Quinteiro <adorador_del_heavy@hotmail.com>, 2004.
# Ignacio Casal Quinteiro <nacho.resa@gmail.com>, 2005, 2006.
# Ignacio Casal Quinteiro <icq@cvs.gnome.org>, 2007.
# Fran Diéguez <frandieguez@ubuntu.com>, 2010.
# Fran Dieguez <frandieguez@gnome.org>, 2009, 2012.
msgid ""
msgstr ""
"Project-Id-Version: gl\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product"
"=epiphany-extensions\n"
"POT-Creation-Date: 2012-11-12 12:38+0100\n"
"PO-Revision-Date: 2012-11-12 12:39+0200\n"
"Last-Translator: Fran Dieguez <frandieguez@gnome.org>\n"
"Language-Team: gnome-l10n-gl@gnome.org\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.1\n"
"X-Project-Style: gnome\n"

#: ../extensions/actions/action-properties.ui.h:1
#: ../extensions/page-info/page-info.glade.h:17
msgid "General"
msgstr "Xeral"

#: ../extensions/actions/action-properties.ui.h:2
msgid "_Name:"
msgstr "_Nome:"

#: ../extensions/actions/action-properties.ui.h:3
msgid "_Description:"
msgstr "_Descrición:"

#: ../extensions/actions/action-properties.ui.h:4
msgid "_Command:"
msgstr "_Orde:"

#: ../extensions/actions/action-properties.ui.h:5
msgid "Applies to"
msgstr "Aplícase a"

#: ../extensions/actions/action-properties.ui.h:6
msgid "_Pages"
msgstr "_Páxinas"

#: ../extensions/actions/action-properties.ui.h:7
msgid "_Images"
msgstr "_Imaxes"

#: ../extensions/actions/actions-editor.ui.h:1
#: ../extensions/actions/actions.ephy-extension.in.in.h:1
msgid "Actions"
msgstr "Accións"

#: ../extensions/actions/ephy-actions-extension.c:114
msgid "Actio_ns"
msgstr "Acció_ns"

#: ../extensions/actions/ephy-actions-extension.c:115
msgid "Customize actions"
msgstr "Accións personalizadas"

#: ../extensions/actions/ephy-actions-extension.c:440
msgid "Could not run command"
msgstr "Non foi posíbel executar a orde"

#: ../extensions/actions/ephy-actions-extension.c:444
msgid "Could not Run Command"
msgstr "Non foi posíbel Executar a orde"

#: ../extensions/actions/ephy-actions-extension-editor-dialog.c:496
msgid "No action selected."
msgstr "Non se seleccionou ningunha acción."

#: ../extensions/actions/ephy-actions-extension-editor-dialog.c:506
#, c-format
msgid "%i action selected."
msgid_plural "%i actions selected."
msgstr[0] "Seleccionouse %i acción."
msgstr[1] "Seleccionouse %i accións."

#: ../extensions/actions/ephy-actions-extension-editor-dialog.c:721
#: ../extensions/extensions-manager-ui/extensions-manager-ui.c:124
#, c-format
msgid "Could not display help: %s"
msgstr "Non foi posíbel mostrar a axua: %s"

#: ../extensions/actions/ephy-actions-extension-properties-dialog.c:274
msgid "Add Action"
msgstr "Engadir acción"

#: ../extensions/actions/ephy-actions-extension-properties-dialog.c:384
#, c-format
msgid "“%s” Properties"
msgstr "Propiedades de “%s”"

#: ../extensions/actions/ephy-actions-extension-properties-dialog.c:389
msgid "Action Properties"
msgstr "Propiedades de acción"

#: ../extensions/auto-reload/ephy-auto-reload-extension.c:80
msgid "_Auto Reload"
msgstr "Recargar _automaticamente"

#: ../extensions/auto-reload/ephy-auto-reload-extension.c:82
msgid "Reload the page periodically"
msgstr "Recargar a páxina periodicamente"

#: ../extensions/certificates/ephy-certificates-extension.c:163
#: ../extensions/certificates/certificates.ephy-extension.in.in.h:1
msgid "Certificates"
msgstr "Certificados"

#: ../extensions/certificates/ephy-certificates-extension.c:194
msgid "Security Devices"
msgstr "Dispositivos de seguranza"

#. stock icon
#: ../extensions/certificates/ephy-certificates-extension.c:206
msgid "Manage _Certificates"
msgstr "Administrar _certificados"

#. shortcut key
#: ../extensions/certificates/ephy-certificates-extension.c:208
msgid "Manage your certificates"
msgstr "Administrar os teus certificados"

#. stock icon
#: ../extensions/certificates/ephy-certificates-extension.c:212
msgid "Manage Security _Devices"
msgstr "Administrar _dispositivos de seguranza"

#. shortcut key
#: ../extensions/certificates/ephy-certificates-extension.c:214
msgid "Manage your security devices"
msgstr "Adiminstrar os seus dispositivos de seguranza"

#: ../extensions/error-viewer/ephy-error-viewer-extension.c:90
msgid "Check _HTML"
msgstr "Verificar _HTML"

#. shortcut key
#: ../extensions/error-viewer/ephy-error-viewer-extension.c:92
msgid "Display HTML errors in the Error Viewer dialog"
msgstr "Mostrar os erros HTML no diálogo do visor de erros"

#: ../extensions/error-viewer/ephy-error-viewer-extension.c:97
msgid "Check _Links"
msgstr "Verificar _ligazons"

#. shortcut key
#: ../extensions/error-viewer/ephy-error-viewer-extension.c:99
msgid "Display invalid hyperlinks in the Error Viewer dialog"
msgstr "Mostra as hiperligazóns no diálogo do visor de erros"

#: ../extensions/error-viewer/ephy-error-viewer-extension.c:103
msgid "_Error Viewer"
msgstr "_Visor de erros"

#. shortcut key
#: ../extensions/error-viewer/ephy-error-viewer-extension.c:105
msgid "Display web page errors"
msgstr "Mostrar os erros da páxina web"

#: ../extensions/error-viewer/error-viewer.glade.h:1
msgid "Error Viewer"
msgstr "Visor de erros"

#: ../extensions/error-viewer/error-viewer.glade.h:2
msgid "Cl_ear"
msgstr "_Limpar"

#. Translators: The first %s is the error category (e. g., "javascript").
#. The second %s is the URL. The third %s is the actual error message.
#: ../extensions/error-viewer/mozilla/ErrorViewerConsoleListener.cpp:105
#, c-format
msgid ""
"%s error in %s on line %d and column %d:\n"
"%s"
msgstr ""
"Erro %s en %s na liña %d e columna %d:\n"
"%s"

#. Translators: The first %s is the error category (e. g., "javascript").
#. The second %s is the URL. The third %s is the actual error message.
#: ../extensions/error-viewer/mozilla/ErrorViewerConsoleListener.cpp:113
#: ../extensions/error-viewer/mozilla/ErrorViewerConsoleListener.cpp:137
#: ../extensions/error-viewer/mozilla/ErrorViewerConsoleListener.cpp:150
#, c-format
msgid ""
"%s error in %s:\n"
"%s"
msgstr ""
"Erro %s en %s:\n"
"%s"

#. Translators: The first %s is the error category (e. g., "javascript").
#. The second %s is the URL. The third %s is the actual error message.
#: ../extensions/error-viewer/mozilla/ErrorViewerConsoleListener.cpp:129
#, c-format
msgid ""
"%s error in %s on line %d:\n"
"%s"
msgstr ""
"Erro %s en %s na liña %d:\n"
"%s"

#: ../extensions/error-viewer/mozilla/ErrorViewerURICheckerObserver.cpp:62
#, c-format
msgid "Found %d invalid link"
msgid_plural "Found %d invalid links"
msgstr[0] "Encontrado %d ligazón inválida"
msgstr[1] "Encontrados %d ligazóns inválidas"

#: ../extensions/error-viewer/mozilla/ErrorViewerURICheckerObserver.cpp:119
#, c-format
msgid ""
"Link error in %s:\n"
"%s is unavailable."
msgstr ""
"Produciuse un erro na ligazón en %s:\n"
"%s non está dispoñíbel."

#: ../extensions/error-viewer/mozilla/ErrorViewerURICheckerObserver.cpp:200
#, c-format
msgid "Checking %d Link on “%s”"
msgid_plural "Checking %d Links on “%s”"
msgstr[0] "Verificando %d ligazón en '%s'"
msgstr[1] "Verificando %d ligazóns en '%s'"

#: ../extensions/error-viewer/opensp/validate.cpp:168
#, c-format
msgid ""
"HTML error in “%s” on line %s:\n"
"%s"
msgstr ""
"Erro HTML en '%s' na liña %s:\n"
"%s"

#: ../extensions/error-viewer/sgml-validator.c:164
#, c-format
msgid "Found %d error"
msgid_plural "Found %d errors"
msgstr[0] "Atoupouse %d erro"
msgstr[1] "Atopáronse %d erros"

#: ../extensions/error-viewer/sgml-validator.c:168
#, c-format
msgid "HTML Validation of “%s” complete"
msgstr "Completouse a validación HTML de “%s”"

#: ../extensions/error-viewer/sgml-validator.c:226
#, c-format
msgid ""
"HTML error in %s:\n"
"%s"
msgstr ""
"Erro HTML en %s:\n"
"%s"

#: ../extensions/error-viewer/sgml-validator.c:228
#: ../extensions/error-viewer/sgml-validator.c:244
msgid ""
"Doctype is “XHTML” but content type is “text/html”.  Use “application/xhtml"
"+xml” instead."
msgstr ""
"O tipo de documento é \"XHTML\" pero o tipo de contido é \"text/html\".  Use "
"\"application/xhtml+xml\" no seu lugar."

#: ../extensions/error-viewer/sgml-validator.c:242
#, c-format
msgid ""
"HTML warning in %s:\n"
"%s"
msgstr ""
"Erro HTML en %s:\n"
"%s"

#: ../extensions/error-viewer/sgml-validator.c:359
#, c-format
msgid "HTML error in %s:"
msgstr "Erro HTML en %s:"

#: ../extensions/error-viewer/sgml-validator.c:367
#, c-format
msgid "Invalid character encoding"
msgstr "Codificación de caracteres inválida"

#: ../extensions/error-viewer/sgml-validator.c:372
#, c-format
msgid "Unknown error while converting to UTF-8"
msgstr "Produciuse un erro descoñecido mentras se convertía a UTF-8"

#: ../extensions/error-viewer/sgml-validator.c:434
#, c-format
msgid ""
"HTML error in %s:\n"
"No valid doctype specified."
msgstr ""
"Erro HTML en %s:\n"
"O tipo de documento especificado non é válido."

#: ../extensions/extensions-manager-ui/extensions-manager-ui.c:349
msgid "Enabled"
msgstr "Activado"

#: ../extensions/extensions-manager-ui/extensions-manager-ui.c:357
#: ../extensions/rss/rss-ui.c:539
msgid "Description"
msgstr "Descrición"

#: ../extensions/extensions-manager-ui/extensions-manager-ui.ui.h:1
msgid "Extensions"
msgstr "Extensións"

#: ../extensions/extensions-manager-ui/extensions-manager-ui.ui.h:2
msgid "Loaded Extensions"
msgstr "Extensións cargadas"

#: ../extensions/extensions-manager-ui/ephy-extensions-manager-ui-extension.c:66
msgid "_Extensions manager"
msgstr "Xestor de _extensións"

#: ../extensions/extensions-manager-ui/ephy-extensions-manager-ui-extension.c:68
msgid "Load and unload extensions"
msgstr "Cargar e descargar extensións"

#: ../extensions/greasemonkey/ephy-greasemonkey-extension.c:78
msgid "Install User _Script"
msgstr "Instalar _script de usuario"

#. shortcut key
#: ../extensions/greasemonkey/ephy-greasemonkey-extension.c:80
msgid "Install Greasemonkey script"
msgstr "Instalar script de Greasemonkey"

#: ../extensions/greasemonkey/ephy-greasemonkey-extension.c:314
#, c-format
msgid "The user script at “%s” has been installed"
msgstr "Instalouse o script de usuario en '%s'"

#: ../extensions/java-console/ephy-java-console-extension.c:48
msgid "_Java Console"
msgstr "Consola _java"

#. shortcut key
#: ../extensions/java-console/ephy-java-console-extension.c:50
msgid "Show Java Console"
msgstr "Mostrar a consola java"

#: ../extensions/livehttpheaders/ephy-livehttpheaders-extension.c:100
msgid "_HTTP Headers"
msgstr "Cabeceiras _HTTP"

#: ../extensions/livehttpheaders/livehttpheaders-ui.glade.h:1
msgid "Live Headers"
msgstr "Cabeceiras en vivo"

#: ../extensions/livehttpheaders/livehttpheaders-ui.glade.h:2
msgid "Clear cache"
msgstr "Limpar caché"

#: ../extensions/livehttpheaders/livehttpheaders-ui.glade.h:3
msgid "Remove headers"
msgstr "Eliminar cabeceiras"

#: ../extensions/livehttpheaders/livehttpheaders-ui.glade.h:4
msgid "URLs"
msgstr "URLs"

#: ../extensions/livehttpheaders/livehttpheaders-ui.glade.h:5
#, fuzzy
msgid "Request"
msgstr "Solicitude"

#: ../extensions/livehttpheaders/livehttpheaders-ui.glade.h:6
msgid "Response"
msgstr "Resposta"

#: ../extensions/page-info/ephy-page-info-extension.c:61
msgid "Pa_ge Information"
msgstr "Información da pá_xina"

#: ../extensions/page-info/ephy-page-info-extension.c:63
msgid "Display page information in a dialog"
msgstr "Mostra a información da páxina nun diálogo"

#: ../extensions/page-info/page-info-dialog.c:615
msgid "Save As..."
msgstr "Gardar como..."

#: ../extensions/page-info/page-info-dialog.c:632
msgid "Select a directory"
msgstr "Seleccionar un directorio"

#: ../extensions/page-info/page-info-dialog.c:681
msgid "Unknown type"
msgstr "Tipo descoñecido"

#: ../extensions/page-info/page-info-dialog.c:686
msgid "Full standards compliance"
msgstr "Compre con todos os estándares"

#: ../extensions/page-info/page-info-dialog.c:689
msgid "Almost standards compliance"
msgstr "Compre coa maioría dos estándares"

#: ../extensions/page-info/page-info-dialog.c:692
msgid "Compatibility"
msgstr "Compatibilidade"

#: ../extensions/page-info/page-info-dialog.c:696
msgid "Undetermined"
msgstr "Indeterminada"

#: ../extensions/page-info/page-info-dialog.c:705
msgid "Not cached"
msgstr "Non cacheado"

#: ../extensions/page-info/page-info-dialog.c:708
msgid "Disk cache"
msgstr "Caché de disco"

#: ../extensions/page-info/page-info-dialog.c:711
msgid "Memory cache"
msgstr "Caché de memoria"

#: ../extensions/page-info/page-info-dialog.c:714
msgid "Unknown cache"
msgstr "Caché descoñecida"

#: ../extensions/page-info/page-info-dialog.c:730
#: ../extensions/page-info/page-info-dialog.c:846
msgid "Unknown"
msgstr "Descoñecido"

#: ../extensions/page-info/page-info-dialog.c:735
msgid "No referrer"
msgstr "Sen referente"

#: ../extensions/page-info/page-info-dialog.c:749
#: ../extensions/page-info/page-info-dialog.c:765
msgid "Not specified"
msgstr "Sen especificar"

#: ../extensions/page-info/page-info-dialog.c:828
msgid "Image"
msgstr "Imaxe"

#: ../extensions/page-info/page-info-dialog.c:831
msgid "Background image"
msgstr "Imaxe de fondo"

#: ../extensions/page-info/page-info-dialog.c:834
msgid "Embed"
msgstr "Embebido"

#: ../extensions/page-info/page-info-dialog.c:837
msgid "Object"
msgstr "Obxecto"

#: ../extensions/page-info/page-info-dialog.c:840
msgid "Applet"
msgstr "Applet"

#: ../extensions/page-info/page-info-dialog.c:843
msgid "Icon"
msgstr "Icona"

#: ../extensions/page-info/page-info-dialog.c:1024
msgid "_Open"
msgstr "_Abrir"

#: ../extensions/page-info/page-info-dialog.c:1030
#: ../extensions/page-info/page-info-dialog.c:1475
msgid "_Copy Link Address"
msgstr "_Copiar enderezo do ligazón"

#: ../extensions/page-info/page-info-dialog.c:1036
msgid "_Use as Background"
msgstr "_Usar como fondo"

#: ../extensions/page-info/page-info-dialog.c:1042
#: ../extensions/page-info/page-info-dialog.c:1481
msgid "_Save As..."
msgstr "_Gardar como..."

#: ../extensions/page-info/page-info-dialog.c:1274
#: ../extensions/page-info/page-info-dialog.c:1595
msgid "Blocked"
msgstr "Bloqueado"

#: ../extensions/page-info/page-info-dialog.c:1290
#: ../extensions/page-info/page-info-dialog.c:1611
msgid "URL"
msgstr "URL"

#: ../extensions/page-info/page-info-dialog.c:1303
msgid "Type"
msgstr "Tipo"

#: ../extensions/page-info/page-info-dialog.c:1315
msgid "Alt Text"
msgstr "Texto Alt"

#: ../extensions/page-info/page-info-dialog.c:1327
#: ../extensions/page-info/page-info-dialog.c:1624
msgid "Title"
msgstr "Título"

#: ../extensions/page-info/page-info-dialog.c:1339
msgid "Width"
msgstr "Ancho"

#: ../extensions/page-info/page-info-dialog.c:1351
msgid "Height"
msgstr "Alto"

#: ../extensions/page-info/page-info-dialog.c:1636
msgid "Relation"
msgstr "Relación"

#: ../extensions/page-info/page-info-dialog.c:1785
#: ../extensions/page-info/page-info-dialog.c:1926
msgid "Name"
msgstr "Nome"

#: ../extensions/page-info/page-info-dialog.c:1797
msgid "Method"
msgstr "Método"

#: ../extensions/page-info/page-info-dialog.c:1811
msgid "Action"
msgstr "Acción"

#: ../extensions/page-info/page-info-dialog.c:1892
msgid "_Copy"
msgstr "_Copiar"

#: ../extensions/page-info/page-info-dialog.c:1938
msgid "Content"
msgstr "Contido"

#: ../extensions/page-info/page-info-dialog.c:2039
msgid "Page Metadata"
msgstr "Metadatos da páxina"

#: ../extensions/page-info/page-info.glade.h:1
msgid "Page Properties"
msgstr "Propiedades da páxina"

#: ../extensions/page-info/page-info.glade.h:2
msgid "Title:"
msgstr "Título:"

#: ../extensions/page-info/page-info.glade.h:3
msgid "Location:"
msgstr "Ubicación:"

#: ../extensions/page-info/page-info.glade.h:4
msgid "Referrer:"
msgstr "Referrer:"

#: ../extensions/page-info/page-info.glade.h:5
msgid "DYNAMIC"
msgstr "DINÁMICO"

#: ../extensions/page-info/page-info.glade.h:6
msgid "Web Page"
msgstr "Páxina web"

#: ../extensions/page-info/page-info.glade.h:7
msgid "Size:"
msgstr "Tamaño:"

#: ../extensions/page-info/page-info.glade.h:8
msgid "Type:"
msgstr "Tipo:"

#: ../extensions/page-info/page-info.glade.h:9
msgid "MIME type:"
msgstr "Tipo MIME:"

#: ../extensions/page-info/page-info.glade.h:10
msgid "Modified:"
msgstr "Modificado:"

#: ../extensions/page-info/page-info.glade.h:11
msgid "Expires:"
msgstr "Caducidade:"

#: ../extensions/page-info/page-info.glade.h:12
msgid "Source:"
msgstr "Orixe:"

#: ../extensions/page-info/page-info.glade.h:13
msgid "Source File"
msgstr "Ficheiro orixe"

#: ../extensions/page-info/page-info.glade.h:14
msgid "Mode:"
msgstr "Modo:"

#: ../extensions/page-info/page-info.glade.h:15
msgid "Encoding:"
msgstr "Codificación:"

#: ../extensions/page-info/page-info.glade.h:16
msgid "Rendering"
msgstr "Renderizado"

#: ../extensions/page-info/page-info.glade.h:18
msgid "Media In Page"
msgstr "Multimedia na páxina"

#: ../extensions/page-info/page-info.glade.h:19
msgid "_Save Media As..."
msgstr "_Gardar medios como..."

#: ../extensions/page-info/page-info.glade.h:20
msgid "Medium View"
msgstr "Vista de medios"

#: ../extensions/page-info/page-info.glade.h:21
msgid "Media"
msgstr "Medios"

#: ../extensions/page-info/page-info.glade.h:22
msgid "Links In Page"
msgstr "Ligazóns na páxina"

#: ../extensions/page-info/page-info.glade.h:23
msgid "Links"
msgstr "Ligazóns"

#: ../extensions/page-info/page-info.glade.h:24
msgid "Forms In Page"
msgstr "Formularios na páxina"

#: ../extensions/page-info/page-info.glade.h:25
msgid "Forms"
msgstr "Formularios"

#: ../extensions/page-info/page-info.glade.h:26
msgid "Dublin Core Metadata"
msgstr "Metadatos do núcleo Dublin"

#: ../extensions/page-info/page-info.glade.h:27
msgid "Date:"
msgstr "Data:"

#: ../extensions/page-info/page-info.glade.h:28
msgid "Format:"
msgstr "Formato:"

#: ../extensions/page-info/page-info.glade.h:29
msgid "Description:"
msgstr "Descrición:"

#: ../extensions/page-info/page-info.glade.h:30
msgid "Other Metadata"
msgstr "Outros metadatos"

#: ../extensions/page-info/page-info.glade.h:31
msgid "Metadata"
msgstr "Metadatos"

#: ../extensions/permissions/ephy-permissions-dialog.c:165
msgid "allowed"
msgstr "permitido"

#: ../extensions/permissions/ephy-permissions-dialog.c:169
msgid "denied"
msgstr "denegado"

#. Sometimes we get this... how did this get in the list!?
#: ../extensions/permissions/ephy-permissions-dialog.c:174
msgid "default"
msgstr "predefinido"

#: ../extensions/permissions/ephy-permissions-dialog.c:500
msgid "Domain"
msgstr "Dominio"

#: ../extensions/permissions/ephy-permissions-dialog.c:512
#: ../extensions/soup-fly/soup-fly.c:196
msgid "State"
msgstr "Estado"

#: ../extensions/permissions/ephy-permissions-dialog.c:605
msgid "Site Permissions"
msgstr "Permisos do sitio"

#: ../extensions/permissions/ephy-permissions-dialog.c:631
msgid "Cookies"
msgstr "Cookies"

#: ../extensions/permissions/ephy-permissions-dialog.c:632
msgid "Images"
msgstr "Imaxes"

#: ../extensions/permissions/ephy-permissions-dialog.c:633
msgid "Popup Windows"
msgstr "Xanelas emerxentes"

#. stock icon
#: ../extensions/permissions/ephy-permissions-extension.c:152
msgid "Site _Permissions"
msgstr "_Permisos do sitio"

#. shortcut key
#: ../extensions/permissions/ephy-permissions-extension.c:154
msgid "Manage web site permissions"
msgstr "Xestionar permisos do sitio web"

#: ../extensions/rss/ephy-rss-extension.c:82
msgid "News Feeds Subscription"
msgstr "Subscrición a proveedores de noticias"

#: ../extensions/rss/ephy-rss-extension.c:84
msgid "Subscribe to this website's news feeds in your favorite news reader"
msgstr ""
"Subscribirse ao proveedor de noticias desta web no seu lector de noticias "
"favorito"

#: ../extensions/rss/ephy-rss-extension.c:89
msgid "_Subscribe to this feed"
msgstr "_Subscribirse a este proveedor"

#: ../extensions/rss/ephy-rss-extension.c:91
msgid "Subscribe to this feed in your favorite news reader"
msgstr ""
"Subscribirse a este proveedor de noticias no seu lector de noticias favorito"

#: ../extensions/rss/ephy-rss-extension.c:402
msgid "Subscribe to site's news feed"
msgstr "Subscribirse ao proveedor de noticias do sitio"

#: ../extensions/rss/rss-ui.c:136
msgid "<b><i>Unable to contact the feed reader, is it running ?</i></b>"
msgstr ""
"<b><i>Non foi posíbel contactar co lector de provedores, estase executando?</"
"i></b>"

#: ../extensions/rss/rss-ui.c:138
msgid "Retry"
msgstr "Reintentar"

#: ../extensions/rss/rss-ui.c:268
msgid "_Copy Feed Address"
msgstr "_Copiar enderezo do proveedor"

#: ../extensions/rss/rss-ui.c:531
msgid "Subscribe"
msgstr "Subscribirse"

#: ../extensions/rss/rss-ui.ui.h:1
#: ../extensions/rss/rss.ephy-extension.in.in.h:1
msgid "News Feed Subscription"
msgstr "Subscrición a provedores de noticias"

#: ../extensions/rss/rss-ui.ui.h:2
msgid "Select Interesting Feeds"
msgstr "Seleccionar fornecedores interesantes"

#: ../extensions/rss/rss-ui.ui.h:3
msgid "_Subscribe"
msgstr "_Subscribirse"

#: ../extensions/select-stylesheet/ephy-css-menu.c:140
msgid "Render the page without using a style"
msgstr "Renderizar a páxina sen usar un estilo"

#: ../extensions/select-stylesheet/ephy-css-menu.c:143
msgid "Render the page using the default style"
msgstr "Renderizar a páxina usando o estilo por defecto"

#: ../extensions/select-stylesheet/ephy-css-menu.c:146
#, c-format
msgid "Render the page using the “%s” style"
msgstr "Renderizar a páxina usando o estilo \"%s\""

#: ../extensions/select-stylesheet/ephy-css-menu.c:317
msgid "St_yle"
msgstr "Est_ilo"

#: ../extensions/select-stylesheet/ephy-css-menu.c:318
msgid "Select a different style for this page"
msgstr "Seleccionar un estilo diferente para esta páxina"

#: ../extensions/select-stylesheet/mozilla/mozilla-helpers.cpp:225
msgid "Default"
msgstr "Por defecto"

#: ../extensions/select-stylesheet/mozilla/mozilla-helpers.cpp:236
msgid "None"
msgstr "Ningún"

#: ../extensions/smart-bookmarks/smart-bookmarks-extension.c:543
msgid "_Look Up"
msgstr "_Buscar"

#: ../extensions/smart-bookmarks/smart-bookmarks-extension.c:550
msgid "_GNOME Dictionary"
msgstr "Diccionario de _Gnome"

#: ../extensions/soup-fly/ephy-soup-fly-extension.c:66
msgid "_Soup Fly"
msgstr "_Soup Fly"

#. shortcut key
#: ../extensions/soup-fly/ephy-soup-fly-extension.c:68
msgid "Show information about the SoupSession used by the browser"
msgstr "Mostrar información sobre SoupSession usada polo navegador"

#: ../extensions/soup-fly/soup-fly.c:220
msgid "Clear finished"
msgstr "Limpar os rematados"

#: ../extensions/soup-fly/soup-fly.c:224
msgid "Automatically remove finished messages"
msgstr "Elminar as mensaxes rematadas automaticamente"

#: ../extensions/actions/actions.ephy-extension.in.in.h:2
msgid "Execute arbitrary commands from the context menu"
msgstr "Executar ordes arbitrarias dende o menú contextual"

#: ../extensions/auto-scroller/auto-scroller.ephy-extension.in.in.h:1
msgid "Auto Scroll"
msgstr "Autodesprazamento"

#: ../extensions/auto-scroller/auto-scroller.ephy-extension.in.in.h:2
msgid "Scroll on middle mouse click"
msgstr "Desprazar ao premer o botón central do rato"

#: ../extensions/auto-reload/auto-reload.ephy-extension.in.in.h:1
msgid "Auto Reload Tab"
msgstr "Recargar o separador automaticamente"

#: ../extensions/auto-reload/auto-reload.ephy-extension.in.in.h:2
msgid "Reload a tab periodically"
msgstr "Recargar o separador periodicamente"

#: ../extensions/certificates/certificates.ephy-extension.in.in.h:2
msgid "View the page certificate and manage the certificates"
msgstr "Vexa o certificado de páxina e xestione os certificados"

#: ../extensions/error-viewer/error-viewer.ephy-extension.in.in.h:1
msgid "Error viewer"
msgstr "Visor de erros"

#: ../extensions/error-viewer/error-viewer.ephy-extension.in.in.h:2
msgid "Validate web pages and check links"
msgstr "Valida as páxinas web e comproba as ligazóns"

#: ../extensions/extensions-manager-ui/extensions-manager-ui.ephy-extension.in.in.h:1
msgid "Extensions Manager"
msgstr "Administrador de extensións"

#: ../extensions/extensions-manager-ui/extensions-manager-ui.ephy-extension.in.in.h:2
msgid "Activate and deactivate extensions"
msgstr "Activar e desactivar extensións"

#: ../extensions/gestures/gestures.ephy-extension.in.in.h:1
msgid "Gestures"
msgstr "Xestos"

#: ../extensions/gestures/gestures.ephy-extension.in.in.h:2
msgid "Perform actions with mouse gestures"
msgstr "Faga accións con xestos do rato"

#: ../extensions/greasemonkey/greasemonkey.ephy-extension.in.in.h:1
msgid "Greasemonkey"
msgstr "Greasemonkey"

#: ../extensions/greasemonkey/greasemonkey.ephy-extension.in.in.h:2
msgid "Run user scripts to modify web pages' behavior"
msgstr ""
"Executar scripts de usuario para modificar o comportamento das páxinas web"

#: ../extensions/java-console/java-console.ephy-extension.in.in.h:1
msgid "Java Console"
msgstr "Consola java"

#: ../extensions/java-console/java-console.ephy-extension.in.in.h:2
msgid "Provide access to the Java-Plugin Console"
msgstr "Proporcionar acceso á consola do engadido de Java"

#: ../extensions/livehttpheaders/livehttpheaders.ephy-extension.in.in.h:1
msgid "Live HTTP headers"
msgstr "Cabeceiras HTTP en vivo"

#: ../extensions/page-info/page-info.ephy-extension.in.in.h:1
msgid "Page Info"
msgstr "Información da páxina"

#: ../extensions/page-info/page-info.ephy-extension.in.in.h:2
msgid "Display information about and links and media from the current page"
msgstr ""
"Mostra información acerca das ligazóns e os medios dende a páxina actual"

#: ../extensions/permissions/permissions.ephy-extension.in.in.h:1
msgid "Permissions"
msgstr "Permisos"

#: ../extensions/permissions/permissions.ephy-extension.in.in.h:2
msgid "Edit site permissions"
msgstr "Editar permisos do sitio"

#: ../extensions/push-scroller/push-scroller.ephy-extension.in.in.h:1
msgid "Push Scroll"
msgstr "Autodesprazamento"

#: ../extensions/push-scroller/push-scroller.ephy-extension.in.in.h:2
msgid "Drag the page on middle mouse click"
msgstr "Desprazar a páxina ao premer o botón central do rato"

#: ../extensions/rss/rss.ephy-extension.in.in.h:2
msgid ""
"Subscribe to a news feed offered by the webpage in your favorite news reader"
msgstr ""
"Subscribirse a provedores de novas ofrecidos pola páxina web no seu lector "
"de noticias favorito"

#: ../extensions/sample/sample.ephy-extension.in.in.h:1
msgid "Sample"
msgstr "Exemplo"

#: ../extensions/sample/sample.ephy-extension.in.in.h:2
msgid "A sample extension"
msgstr "Unha extensión de exemplo"

#: ../extensions/select-stylesheet/select-stylesheet.ephy-extension.in.in.h:1
msgid "Select Stylesheet"
msgstr "Seleccione a folla de estilo"

#: ../extensions/select-stylesheet/select-stylesheet.ephy-extension.in.in.h:2
msgid "Select between different stylesheets offered by the web page"
msgstr ""
"Seleccionar entre diferentes follas de estilo ofrecidas pola páxina web"

#: ../extensions/smart-bookmarks/smart-bookmarks.ephy-extension.in.in.h:1
msgid "Smart Bookmarks"
msgstr "Marcadores intelixentes"

#: ../extensions/smart-bookmarks/smart-bookmarks.ephy-extension.in.in.h:2
msgid "Look up the selected text with a smart bookmark"
msgstr "Busca o texto seleccionado cun marcador intelixente"

#: ../extensions/tab-states/tab-states.ephy-extension.in.in.h:1
msgid "Tab States"
msgstr "Estado dos separadores"

#: ../extensions/tab-states/tab-states.ephy-extension.in.in.h:2
msgid "Indicates new content in background tabs"
msgstr "Indica o contido novo en separadores en segundo plano"

#: ../extensions/tab-key-tab-navigate/tab-key-tab-navigate.ephy-extension.in.in.h:1
msgid "Tab Key Tab Navigate"
msgstr "Tecla de tabulación para navegar polos separadores"

#: ../extensions/tab-key-tab-navigate/tab-key-tab-navigate.ephy-extension.in.in.h:2
msgid "Use the Tab key to navigate between tabs"
msgstr "Use a tecla de tabulación para navegar entre os separadores"

#~ msgid "<span weight=\"bold\">General</span>"
#~ msgstr "<span weight=\"bold\">Xeral</span>"

#~ msgid "<span weight=\"bold\">Applies to</span>"
#~ msgstr "<span weight=\"bold\">Aplícase a</span>"

#~ msgid "Invalid filter"
#~ msgstr "Filtro non válido"

#~ msgid "Filter URI"
#~ msgstr "Filtrar URI"

#~ msgid "Adblock Editor"
#~ msgstr "Editor Adblock"

#~ msgid ""
#~ "Type the address of a preconfigured filter list in the text entry to be "
#~ "added to the list. Find more lists at easylist.adblockplus.org"
#~ msgstr ""
#~ "Escriba o enderezo dunha lista de filtros preconfigurada na caixa de "
#~ "texto para engadila á lista. Pode atopar máis listas en easylist."
#~ "adblockplus.org"

#~ msgid "Ad Blocker"
#~ msgstr "Bloqueador de publicidade"

#~ msgid "Configure Ad Blocker filters"
#~ msgstr "Configurar os filtros do bloqueador de publicidade"

#~ msgid "<b>URLs</b>"
#~ msgstr "<b>URLs</b>"

#~ msgid "Ad Blocker v2"
#~ msgstr "Bloqueador de publicidade v2"

#~ msgid "Block the pest!"
#~ msgstr "Bloquea a peste!"

#~ msgid ""
#~ "This list is Copyright © its original author(G). It is relicensed under "
#~ "the GPL with permission.\n"
#~ "The original list, Filterset.G, can be found at http://www.pierceive.com/"
#~ "filtersetg/.\n"
#~ "This version has been modified from its original. The changes have not "
#~ "been approved by the original author;\n"
#~ "direct any problems to Bugzilla and not to the original author (G).\n"
#~ msgstr ""
#~ "Esta lista é o Copyright © o seu autor orixinal(G). Está baixo a licenza "
#~ "baixo a GPL con permiso.\n"
#~ "A lista orixinal, Filterset G, pode encontrarse en http://www.pierceive."
#~ "com/filtersetg/.\n"
#~ "Esta versión foi modificada polo seu orixinal. Os cambios non foron "
#~ "aprobados polo seu autor orixinal.\n"
#~ "directamente calquera problema a Bugzilla e non ao seu autor orixinal "
#~ "(G).\n"

#~ msgid "Pattern"
#~ msgstr "Patrón"

#~ msgid "<b>Courtesy of Graham Pierce</b>"
#~ msgstr "<b>Cortesía de Graham Pierce</b>"

#~ msgid "<b>New rule</b>"
#~ msgstr "<b>Nova regra</b>"

#~ msgid "<b>Update rules from original site</b>"
#~ msgstr "<b>Actualizar regras dende o sitio orixinal</b>"

#~ msgid "Blacklist"
#~ msgstr "Lista negra"

#~ msgid "Filterset.G"
#~ msgstr "Filterset.G"

#~ msgid "Whitelist"
#~ msgstr "Lista branca"

#~ msgid "Edit Adblock"
#~ msgstr "Editar Adblock"

#~ msgid "%d hidden advertisement"
#~ msgid_plural "%d hidden advertisements"
#~ msgstr[0] "%d anuncio agochado"
#~ msgstr[1] "%d anuncios agochados"

#~ msgid "_Extensions"
#~ msgstr "_Extensións"

#~ msgid "Help about extensions"
#~ msgstr "Axuda acerca das extensións"

#~ msgid "Open search result in new tab"
#~ msgstr "Abrir resultados da busqeuda nun novo separador"

#~ msgid "Open search result in new tab if true, in new window if false"
#~ msgstr "Abrir marcadores nun separador novo da xanela activa"

#~ msgid "Groups newly opened tabs"
#~ msgstr "Agrupa os separadores novos abertos"

#~ msgid "Tab Groups"
#~ msgstr "Grupos de separadores"

#~ msgid ""
#~ "If this option is enabled, the action will be included in the popup menu "
#~ "of images."
#~ msgstr ""
#~ "Se esta opción está activada, a acción será incluída no menú emerxente de "
#~ "imaxes."

#~ msgid ""
#~ "If this option is enabled, the action will be included in the popup menu "
#~ "of links and pages."
#~ msgstr ""
#~ "Se está activada esta opción, a acción será incluída no menú emerxente de "
#~ "ligazóns e páxinas."

#~ msgid "The action description."
#~ msgstr "A descrición da acción."

#~ msgid ""
#~ "The action name. If a character is preceded by an underscore, that "
#~ "character will be used as the access key for the menu item."
#~ msgstr ""
#~ "O nome da acción. Se se precede un carácter con un guión baixo, ese "
#~ "carácter usarase para a combinación de teclas para o elemento do menú."

#~ msgid ""
#~ "The command to execute when the action is activated. The URL of the link, "
#~ "image or page will be appended to the command."
#~ msgstr ""
#~ "A orde que se vai a executar cando se active a acción. A URL da ligazón, "
#~ "imaxe ou páxina será agregada á orde."

#~ msgid "Add a new rule"
#~ msgstr "Engadir unha nova regra"

#~ msgid "No Sidebars installed"
#~ msgstr "Non hai ningunha barra lateral instalada"

#~ msgid "_Sidebar"
#~ msgstr "Barra _lateral"

#~ msgid "Show or hide the sidebar"
#~ msgstr "Mostrar ou ocultar a barra lateral"

#~ msgid "Do you really want to remove this sidebar?"
#~ msgstr "Está seguro que quere eliminar a barra lateral?"

#~ msgid "There is no way to recover this sidebar after removal."
#~ msgstr "Non hai forma de recuperar esta barra lateral logo de eliminala."

#~ msgid "Remove Sidebar"
#~ msgstr "Eliminar barra lateral"

#~ msgid "The sidebar already contains “%s”"
#~ msgstr "A barra lateral xa contén “%s”"

#~ msgid ""
#~ "To use this sidebar, select it from the list of available sidebar pages."
#~ msgstr ""
#~ "Para empregar esta barra lateral, seleccionea dende a lista das páxinas "
#~ "de barras laterais dispoñibeis."

#~ msgid "Add Sidebar"
#~ msgstr "Engadir barra lateral"

#~ msgid "Add “%s” to the Sidebar?"
#~ msgstr "Engadir \"%s\" á barra lateral?"

#~ msgid "The source to the new sidebar page is “%s”."
#~ msgstr "A fonte da páxina nova da barra lateral é \"%s\"."

#~ msgid "_Add Sidebar"
#~ msgstr "_Engadir barra lateral"

#~ msgid "Adds a sidebar to windows"
#~ msgstr "Engada unha barra lateral ás xanelas"

#~ msgid "Sidebar"
#~ msgstr "Barra lateral"

#~ msgid "View Creative Commons license"
#~ msgstr "Ver a licenza Creative Commons"

#~ msgid "_Exclude"
#~ msgstr "_Excluír"

#~ msgid "_Include"
#~ msgstr "_Incluír"

#~ msgid "_Password:"
#~ msgstr "_Contrasinal:"

#~ msgid "_Tag:"
#~ msgstr "E_tiqueta:"

#~ msgid "Synchronizing..."
#~ msgstr "Sincronizando..."

#~ msgid "del.icio.us password"
#~ msgstr "contrasinal de del.icio.us"

#~ msgid "del.icio.us username"
#~ msgstr "usuario de del.icio.us"

#~ msgid ""
#~ "Error:\n"
#~ "%s"
#~ msgstr ""
#~ "Erro:\n"
#~ "%s"

#~ msgid "_Extensions..."
#~ msgstr "_Extensións..."

#~ msgid "Favicon fallback"
#~ msgstr "Recuperación de Favicon"

#~ msgid ""
#~ "Loads /favicon.ico as favicon if the site doesn't use the standard method"
#~ msgstr ""
#~ "Carga /favicon.ico como favicon se o sitio non usa o método estándar"

#~ msgid "A sample extension with mozilla backend"
#~ msgstr "Unha extensión de exemplo co backend de Mozilla"

#~ msgid "Sample (Mozilla)"
#~ msgstr "Exemplo (Mozilla)"
